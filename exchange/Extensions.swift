//
//  Extensions.swift
//  exchange
//
//  Created by Руслан Ольховка on 01.03.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - RX

extension ObservableType where Self.E == Data {
	public func mapTo<T: Decodable>(_ type: T.Type) -> Observable<T> {
		return flatMap { data -> Observable<T> in
			let object = try JSONDecoder().decode(T.self, from: data)
			return Observable.just(object)
		}
	}
}

// MARK: - Fotmatters

extension NumberFormatter {
	static let twoFractionDigits: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.decimalSeparator = ","
		formatter.groupingSeparator = " "
		formatter.minimumFractionDigits = 2
		formatter.maximumFractionDigits = 2
		return formatter
	}()
	static let intFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.decimalSeparator = ","
		formatter.groupingSeparator = " "
		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 0
		return formatter
	}()
}

protocol Formattable {
	var formatted2: String { get }
	var formatted0: String { get }
}
extension Formattable {
	var formatted2: String {
		return NumberFormatter.twoFractionDigits.string(for: self) ?? ""
	}
	var formatted0: String {
		return NumberFormatter.intFormatter.string(for: self) ?? ""
	}
}
extension Int: Formattable {}
extension Double: Formattable {}

// MARK: - UI

protocol Nibable {
	static func getNib() -> UINib
}
extension Nibable {
	static func getNib() -> UINib {
		let nibName = "\(self)"
		let bundle = Bundle(for: self as! AnyClass)
		return UINib(nibName: nibName, bundle: bundle)
	}
}

