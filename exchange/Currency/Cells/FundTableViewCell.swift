//
//  FundTableViewCell.swift
//  exchange
//
//  Created by Руслан Ольховка on 01.03.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class FundTableViewCell: UITableViewCell {

	@IBOutlet weak var nameLbl: UILabel!
	@IBOutlet weak var volumeLbl: UILabel!
	@IBOutlet weak var priceLbl: UILabel!
	
	var fund: Fund! {
		didSet {
			nameLbl.text = fund.name
			volumeLbl.text = fund.volume.formatted0
			priceLbl.text = fund.price.amount.formatted2
		}
	}
}

extension FundTableViewCell: Nibable {}
