//
//  CurrencyModel.swift
//  exchange
//
//  Created by Руслан Ольховка on 01.03.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import Foundation

struct Stocks: Decodable {
	let stock: [Fund]
}

struct Fund: Decodable {
	let name: String
	let price: Price
	let volume: Int
}

struct Price: Decodable {
	let amount: Double
}
