//
//  CurrencyVM.swift
//  exchange
//
//  Created by Руслан Ольховка on 01.03.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import Foundation
import RxSwift

fileprivate extension URLRequest {
	static let stocks = URLRequest(url: URL(string: "http://phisix-api3.appspot.com/stocks.json")!)
}

class CurrencyVM {
	
	enum Status {
		case success
		case error(String)
	}
	
	lazy var tap = PublishSubject<()>()
	lazy var status = PublishSubject<Status>()
	
	private lazy var timer = {
		Observable<Int8>
			.timer(0, period: 15, scheduler: MainScheduler.asyncInstance)
			.map { _ in () }
	}()
	
	lazy var stocks = {
		Observable.merge([tap, timer])
			.debounce(0.1, scheduler: MainScheduler.asyncInstance)
			.flatMapLatest {_ in
				URLSession.shared.rx
					.data(request: .stocks)
					.mapTo(Stocks.self)
					.map { $0.stock }
					.do(onNext: { [unowned self] _ in
						self.status.onNext(.success)
						},
						onError: { [unowned self] (error) in
							self.status.onNext(.error(error.localizedDescription))
					})
					.asDriver(onErrorJustReturn: [])
		}
	}()
}
