//
//  CurrencyVC.swift
//  exchange
//
//  Created by Руслан Ольховка on 01.03.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CurrencyVC: UIViewController {

	@IBOutlet weak var refreshBtn: UIBarButtonItem!
	@IBOutlet weak var table: UITableView!
	
	let viewModel = CurrencyVM()
	
	let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		refreshBtn.rx.tap
			.bind(to: viewModel.tap)
			.disposed(by: disposeBag)
		
		table.backgroundColor = #colorLiteral(red: 0.1481391042, green: 0.1481391042, blue: 0.1481391042, alpha: 1)
		table.register(FundTableViewCell.getNib(), forCellReuseIdentifier: "Cell")
		viewModel.stocks
			.bind(to: table.rx.items(cellIdentifier: "Cell",
									 cellType: FundTableViewCell.self))
			{ (row, fund, cell) in
				cell.fund = fund
			}
			.disposed(by: disposeBag)
		
		viewModel.status
			.observeOn(MainScheduler.instance)
			.subscribe(onNext: onStatusChange)
			.disposed(by: disposeBag)
	}
	
	func onStatusChange(status: CurrencyVM.Status) {
		switch status {
		case .success:
			self.table.backgroundView = nil
			self.table.separatorStyle = .singleLine
		case .error(let message):
			let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: table.bounds.size.width, height: table.bounds.size.height))
			messageLabel.text = message
			messageLabel.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
			messageLabel.numberOfLines = 0;
			messageLabel.textAlignment = .center
			messageLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
			
			table.backgroundView = messageLabel;
			table.separatorStyle = .none
		}
	}
}

